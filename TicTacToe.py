# -*- coding: utf-8 -*-
"""
Created on Wed Oct 2 15:40:44 2019
@author: Annelore Visker
Icon made by Freepik from www.flaticon.com (7-Oct-2019)
"""

# ----- GLOBAL VARS -----

# Initial map
map = ["-","-","-","-","-","-","-","-","-"]
# Initial winner
winner = None
# Initial game status (go=True vs stop=False)
no_win_tie = True
# Initial current player
cp = "X"

# ----- DEFINITIONS -----
def play():
    disp_map() # Initial map
    # Game on
    while no_win_tie:
        handle_turn(cp) # Ask player for input
        disp_map()      # Update map
        check_win_tie() # Game over?
        flip_player()   # Switch player
    # Game over    
    if winner == "X" or winner == "O":
        print(winner + " won!")
    elif winner == None:
        print("Tie!")
    return

def disp_map():                # Display updated playing canvas-map (XXX)
    print(map[0] + "|" + map[1] + "|" + map[2])
    print(map[3] + "|" + map[4] + "|" + map[5])
    print(map[6] + "|" + map[7] + "|" + map[8])
    return

def handle_turn(P):            # Process turn (Ask player & player-input)
    print(P + "'s turn")
    pos = input("Choose a position from 1-9: ")
    invalid = True
    while invalid:
        # Error-msg if invalid input
        while pos not in ["1","2","3","4","5","6","7","8","9"]:
            pos = input("Invalid input. Choose a position from 1-9: ")
            
        pos = int(pos) - 1 # From string to integer (incl. index map list)
        # Error-msg if overwriting (X vs O)
        if map[pos] == "-":
            invalid = False
        else:
            print("Impossible. Overwritten.")   
    map[pos] = P  # Update sign @ map index = pos
    return

def check_win_tie():           # Game over? (Check for win or tie)
    check_win()
    check_tie()  # CHANGE: ADD IF WIN THEN SKIP TIE
    return

def check_win():
    global winner
    R_winner = check_rows()
    C_winner = check_cols()
    D_winner = check_diags()
    if R_winner:
        winner = R_winner
    elif C_winner:
        winner = C_winner
    elif D_winner:
        winner = D_winner
    else:
        winner = None    
    return

def check_tie():
    global no_win_tie
    if "-" not in map:
        no_win_tie = False    
    return

def flip_player():             # Switch between players (X vs O)
    global cp
    if cp == "X":
        cp = "O"
    elif cp == "O":
        cp = "X"
    return

def check_diags():             # Check diagonal match
    global no_win_tie
    D1 = map[0] == map[4] == map[8] != "-"  # Check diagonal 1 (L-Up to R-Down) 
    D2 = map[2] == map[4] == map[6] != "-"  # Check diagonal 2 (R-Up to L-Down) 
    if D1 or D2:
        no_win_tie = False  # Flag win
    # Return winner
    if D1:
        return map[0]
    if D2:
        return map[2]
    return

def check_rows():              # Check row match
    global no_win_tie
    R1 = map[0] == map[1] == map[2] != "-"  # Check row 1 (High)
    R2 = map[3] == map[4] == map[5] != "-"  # Check row 2 (Middle)
    R3 = map[6] == map[7] == map[8] != "-"  # Check row 3 (Low)
    if R1 or R2 or R3:
        no_win_tie = False  # Flag win
    # Return winner 
    if R1:
        return map[0]
    elif R2:
        return map[3]
    elif R3:
        return map[6]
    return

def check_cols():              # Check column match
    global no_win_tie
    C1 = map[0] == map[3] == map[6] != "-"  # Check row 1 (Left)
    C2 = map[1] == map[4] == map[7] != "-"  # Check row 2 (Middle)
    C3 = map[2] == map[5] == map[8] != "-"  # Check row 3 (Right)
    if C1 or C2 or C3:
        no_win_tie = False  # Flag win
    # Return winner
    if C1:
        return map[0]
    elif C2:
        return map[1]
    elif C3:
        return map[2]
    return

# ----- PLAY TIC-TAC-TOE -----
play() 
