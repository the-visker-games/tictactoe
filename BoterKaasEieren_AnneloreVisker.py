# -*- coding: utf-8 -*-
"""
Created on Sat Oct  5 16:01:12 2019
@author: Annelore Visker
Icon made by Freepik from www.flaticon.com (7-Oct-2019)
"""

from PyQt5.QtWidgets import QDialog, QTableWidgetItem,QTableWidget, QHBoxLayout,QVBoxLayout, QInputDialog,QPushButton, QApplication
from PyQt5 import QtGui
import numpy as np
import sys

class PlayTicTacToe(QDialog):
    def __init__(self,name):
        super(PlayTicTacToe, self).__init__()
        rowcolcount = 3                                                  # Largeness game board (#Rows = #Cols)
        self.name = name                                                 # Game name (main)
        self.map = np.array([["-","-","-"],["-","-","-"],["-","-","-"]]) # Initial board status (Help-tool to check for win/tie)
        self.winner = None                                               # Initial winner
        self.no_win_tie = True                                           # Initial game status (Go=>True vs Stop=>False)
        self.cp = "X"                                                    # Initial current player
        
        self.GUIboard(rowcolcount)  # Create GUI game board
        self.GUIbuttons()           # Create GUI push buttons 
        self.GUIlayout()            # Create layout for GUI window   
        self.GUIsettings()          # Customize GUI window settings
        
    def GUIboard(self,NRrowcol):   
        self.tableWidget = QTableWidget()
        self.tableWidget.setRowCount(NRrowcol)
        self.tableWidget.setColumnCount(NRrowcol)
        for col in (0,1,2):
            self.tableWidget.setColumnWidth(col, 50)
        
    def GUIbuttons(self):   # Add button actions & Insert into layout
        self.vboxbuttons = QVBoxLayout()
        for text, slot in (("Next player",self.play), ("Close",self.close)):
            button = QPushButton(text)
            self.vboxbuttons.addWidget(button)
            button.clicked.connect(slot)
        
    def GUIlayout(self):   # Combine board & buttons
        hbox = QHBoxLayout()
        hbox.addWidget(self.tableWidget)
        hbox.addLayout(self.vboxbuttons)
        self.setLayout(hbox)
        
    def GUIsettings(self):  
        self.setWindowTitle("Boter, Kaas & Eieren".format(self.name))
        self.setWindowIcon(QtGui.QIcon("tic-tac-toe.png"))
        
    def play(self):       # Play turn
        if self.no_win_tie:
            print(self.cp + "'s turn. No win/tie.")
        items = ("11","12","13","21","22","23","31","32","33")
        title = "Choose a tile {0}".format(self.name)
        item, ok = QInputDialog.getItem(self, title,"Tile:", items, 0, False)
        row_item = int(item[0]) - 1 # From string to integer (incl. index table)
        col_item = int(item[1]) - 1 # From string to integer (incl. index table)
        if ok and item is not None:
            # Game on
            if self.no_win_tie:
                self.tableWidget.setItem(row_item,col_item,QTableWidgetItem(self.cp))  # Update table (Play turn)
                self.map[row_item,col_item] = self.cp                                  # Update board status (Help-tool for check win/tie)
                self.check_win_tie()                                                   # Game over? (Win vs Tie)
                self.flip_player()                                                     # Switch between players (X vs O)
            # Game over
            elif self.no_win_tie == False:
                if self.winner == "X" or self.winner == "O":      
                    print(self.winner + " won!")
                elif self.winner == None:
                    print("Tie!")
                
    def flip_player(self):             
        if self.cp == "X":
            self.cp = "O"
        elif self.cp == "O":
            self.cp = "X"
    
    def check_win_tie(self):
        self.check_win()
        self.check_tie()

    def check_win(self):
        R_winner = self.check_rows()
        C_winner = self.check_cols()
        D_winner = self.check_diags()
        if R_winner:
            self.winner = R_winner
        elif C_winner:
            self.winner = C_winner
        elif D_winner:
            self.winner = D_winner
        else:
            self.winner = None    
    
    def check_tie(self):
        if "-" not in self.map:
            self.no_win_tie = False    
            
    def check_diags(self):             # Check diagonal match
        D1 = self.map[0,0] == self.map[1,1] == self.map[2,2] != "-"  # Check diagonal 1 (L-Up to R-Down) 
        D2 = self.map[2,0] == self.map[1,1] == self.map[0,2] != "-"  # Check diagonal 2 (R-Up to L-Down) 
        if D1 or D2:
            self.no_win_tie = False  # Flag win
        # Return winner
        if D1:
            return self.map[0]
        elif D2:
            return self.map[2]
        return

    def check_rows(self):              # Check row match
        R1 = self.map[0,0] == self.map[0,1] == self.map[0,2] != "-"  # Check row 1 (High)
        R2 = self.map[1,0] == self.map[1,1] == self.map[1,2] != "-"  # Check row 2 (Middle)
        R3 = self.map[2,0] == self.map[2,1] == self.map[2,2] != "-"  # Check row 3 (Low)
        if R1 or R2 or R3:
            self.no_win_tie = False  # Flag win
        # Return winner 
        if R1:
            return self.map[0,0]
        elif R2:
            return self.map[1,0]
        elif R3:
            return self.map[2,0]
        return

    def check_cols(self):              # Check column match
        C1 = self.map[0,0] == self.map[1,0] == self.map[2,0] != "-"  # Check row 1 (Left)
        C2 = self.map[0,1] == self.map[1,1] == self.map[2,1] != "-"  # Check row 2 (Middle)
        C3 = self.map[0,2] == self.map[1,2] == self.map[2,2] != "-"  # Check row 3 (Right)
        if C1 or C2 or C3:
            self.no_win_tie = False  # Flag win
        # Return winner
        if C1:
            return self.map[0,0]
        elif C2:
            return self.map[0,1]
        elif C3:
            return self.map[0,2]
        return        
    
    def close(self):
        self.accept()

if __name__ == "__main__":
 
    app = QApplication(sys.argv)
    dialog = PlayTicTacToe("BoterKaasEieren")
    dialog.exec_()
    